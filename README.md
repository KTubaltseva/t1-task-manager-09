# TASK MANAGER

## DEVELOPER
**NAME:** Tubaltseva Ksenia

**E-MAIL:** ktubaltseva@t1-consulting.ru

**URL:** https://gitlab.com/KTubaltseva

## SOFTWARE
**OS:** Windows 10

**JDK:** OpenJDK 1.8.0_322

## HARDWARE
**CPU:** i5

**RAM:** 8Gb

**SSD:** 256Gb

## BUILD PROGRAM
```bash
mvn clean install
```

## RUN APPLICATION
````shell
java -jar ./task-manager.jar
````

package ru.t1.ktubaltseva.tm.component;

import ru.t1.ktubaltseva.tm.api.ICommandController;
import ru.t1.ktubaltseva.tm.api.ICommandRepository;
import ru.t1.ktubaltseva.tm.api.ICommandService;
import ru.t1.ktubaltseva.tm.constant.ArgumentConst;
import ru.t1.ktubaltseva.tm.constant.CommandConst;
import ru.t1.ktubaltseva.tm.controller.CommandController;
import ru.t1.ktubaltseva.tm.repository.CommandRepository;
import ru.t1.ktubaltseva.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(String[] args) {
        processArgument(args);
        processCommands();
    }

    private void processArgument(final String[] args) {
        if (args == null || args.length < 1) return;
        final String argument = args[0];
        switch (argument) {
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.INFO:
                commandController.displaySystemInfo();
                break;
            default:
                commandController.displayArgumentError();
        }
        System.exit(0);
    }

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.VERSION:
                commandController.displayVersion();
                break;
            case CommandConst.ABOUT:
                commandController.displayAbout();
                break;
            case CommandConst.HELP:
                commandController.displayHelp();
                break;
            case CommandConst.INFO:
                commandController.displaySystemInfo();
                break;
            case CommandConst.EXIT:
                commandController.exit();
                break;
            default:
                commandController.displayCommandError();
        }
    }

    private void processCommands() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

}

package ru.t1.ktubaltseva.tm.repository;

import ru.t1.ktubaltseva.tm.api.ICommandRepository;
import ru.t1.ktubaltseva.tm.constant.ArgumentConst;
import ru.t1.ktubaltseva.tm.constant.CommandConst;
import ru.t1.ktubaltseva.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            CommandConst.HELP,
            ArgumentConst.HELP,
            "Display list of commands."
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION,
            ArgumentConst.VERSION,
            "Display program version."
    );

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT,
            ArgumentConst.ABOUT,
            "Display developer info."
    );

    private static final Command INFO = new Command(
            CommandConst.INFO,
            ArgumentConst.INFO,
            "Display system info."
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT,
            null,
            "Close Application."
    );

    private static final Command[] COMMANDS = new Command[] {
            HELP,
            VERSION,
            ABOUT,
            INFO,
            EXIT
    };

    @Override
    public Command[] getCommands() {
        return COMMANDS;
    }

}

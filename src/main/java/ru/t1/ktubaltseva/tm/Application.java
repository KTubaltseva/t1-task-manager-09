package ru.t1.ktubaltseva.tm;

import ru.t1.ktubaltseva.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}